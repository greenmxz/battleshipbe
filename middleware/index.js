var fs = require('fs');

var middleware = {};

fs.readdirSync(__dirname + '/').forEach(function(file) {
    var fileName = file.replace('.js', '');

    if (fileName !== 'index') {
        middleware[fileName] = require('./' + fileName);
    }

});

module.exports = middleware;