const sql = require('../util/db')
const util = require('../util/util')
function Authorization (req, res, next) {
	if (req.headers && req.headers.authorization && req.hs_user_id ){
		sql.query(
			"SELECT * FROM hs_token where hs_user_id = ? and token = ? ",
			[req.hs_user_id,req.headers.authorization.split(' ')[1]],
			function (err, result) {
				console.log("RESULT",result)
				if (result && result.length>0) {
					next();
				} else {
					util.sendErrorResponse(res, 401, 'Not authorized', "Internal error")
				}
			})
	}else{
		util.sendErrorResponse(res, 401, 'Not authorized', "Internal error")
	}
}

module.exports = Authorization