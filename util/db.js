const mysql = require('mysql')
const config = require('../config/config')

let connection = mysql.createConnection(config.db)

connection.connect((err) => {
    if(err) throw err
})

module.exports = connection