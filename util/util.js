
const sendErrorResponse = (res, code, errorMessage, extra) => {
    res.status(code)
    return res.json({
        type: 'error',
        err: true,
        message: errorMessage,
        extra: extra
    })
}

const serverError = (req, res) => {
    let statusCode = req.statusCode || 500
    return sendErrorResponse(res, statusCode,'Not')
}

const sendServerResponse = (res, status, type, message, data) => {
    return res.json({
        type: type,
        status: status,
        message: message,
        data: data
    })
}

exports.sendErrorResponse = sendErrorResponse
exports.serverError = serverError
exports.sendServerResponse = sendServerResponse