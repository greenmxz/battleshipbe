const util = require('../util/util')
const authModel = require('../model/auth')
const crypto = require('crypto')


exports.createToken = (request, response) => {
    let {email, password} = request.body
    password = getHashedPassword(password)

    authModel.createToken(email, password, (err, result)=> {
        if (err) {
            util.sendErrorResponse(response, err, result, err)
        }
        else{
            util.sendServerResponse(response, 200, 'token', 'OK',result)
        }
    })
}
exports.isLogged = (request, response) => {
    authModel.isLogged(request, (err, result)=> {
        if (err) {
            util.sendErrorResponse(response, err, result, err)
        }
        else{
            util.sendServerResponse(response, 200, 'token', 'OK',result)
        }
    })
}

const getHashedPassword = (password) => {
    const sha256 = crypto.createHash('sha256');
    const hash = sha256.update(password).digest('base64');
    return hash;
}

