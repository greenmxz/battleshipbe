let util = require('../util/util')
let testModel = require('../model/test')


exports.get = (request, response) => {
    testModel.get( (err, result)=> {
        if (err) {
            util.sendErrorResponse(response, 400, 'Unable to get common', err)
        }
        else{
            util.sendServerResponse(response, 200, 'get_common', result)
        }
    })
}

