let util = require('../util/util')
let gameModel = require('../model/game')


exports.getDifficulty = (request, response) => {
    gameModel.getDifficulty( (err, result)=> {
        if (err) {
            console.log("....",err )
            util.sendErrorResponse(response, 400, 'Unable to get difficulty', err)
        }
        else{
            util.sendServerResponse(response, 200, 'get_difficulty', 'OK', result)
        }
    })
}

exports.getSettings = (request, response) => {
    gameModel.getSettings(  request.hs_user_id, (err, result)=> {
        if (err) {
            util.sendErrorResponse(response, 400, 'Unable to get settings', err)
        }
        else{
            util.sendServerResponse(response, 200, 'get_settings', 'OK', result)
        }
    })
}

exports.updateSettings = (request, response) => {
    gameModel.updateSettings( request.hs_user_id, request.body, (err, result)=> {
        if (err) {
            util.sendErrorResponse(response, 400, 'Unable to update settings', err)
        }
        else{
            util.sendServerResponse(response, 200, 'update_settings', 'OK', result)
        }
    })
}

exports.saveGame = (request, response) => {
    gameModel.saveGame( request.hs_user_id, request.body, (err, result)=> {
        if (err) {
            util.sendErrorResponse(response, 400, 'Unable to save game', err)
        }
        else{
            util.sendServerResponse(response, 200, 'save_games', 'OK', result)
        }
    })
}

exports.getScores = (request, response) => {
    gameModel.getScores( request.hs_user_id, (err, result)=> {
        if (err) {
            util.sendErrorResponse(response, 400, 'Unable to save game', err)
        }
        else{
            util.sendServerResponse(response, 200, 'save_games', 'OK', result)
        }
    })
}



