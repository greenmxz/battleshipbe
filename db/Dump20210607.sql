-- MySQL dump 10.13  Distrib 8.0.23, for Win64 (x86_64)
--
-- Host: localhost    Database: battleship
-- ------------------------------------------------------
-- Server version	8.0.23

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- -----------------------------------------------------
-- Schema battleship
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `battleship` ;

-- -----------------------------------------------------
-- Schema battleship
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `battleship` DEFAULT CHARACTER SET utf8 ;
USE `battleship` ;
--
-- Table structure for table `finished_games`
--

DROP TABLE IF EXISTS `finished_games`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `finished_games` (
  `finished_games_id` int NOT NULL AUTO_INCREMENT,
  `hs_user_id` int DEFAULT NULL,
  `score` varchar(20) DEFAULT NULL,
  `turns` int DEFAULT NULL,
  PRIMARY KEY (`finished_games_id`),
  KEY `fk_finished_games_hs_user1_idx` (`hs_user_id`),
  CONSTRAINT `fk_finished_games_hs_user1` FOREIGN KEY (`hs_user_id`) REFERENCES `hs_user` (`hs_user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `finished_games`
--

LOCK TABLES `finished_games` WRITE;
/*!40000 ALTER TABLE `finished_games` DISABLE KEYS */;
INSERT INTO `finished_games` VALUES (20,1,'Win',85);
/*!40000 ALTER TABLE `finished_games` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `game_difficulty`
--

DROP TABLE IF EXISTS `game_difficulty`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `game_difficulty` (
  `game_difficulty_id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `value` int DEFAULT NULL,
  PRIMARY KEY (`game_difficulty_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `game_difficulty`
--

LOCK TABLES `game_difficulty` WRITE;
/*!40000 ALTER TABLE `game_difficulty` DISABLE KEYS */;
INSERT INTO `game_difficulty` VALUES (1,'Easy',-1),(2,'Medium',100),(3,'Hard',50);
/*!40000 ALTER TABLE `game_difficulty` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hs_token`
--

DROP TABLE IF EXISTS `hs_token`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `hs_token` (
  `hs_token_id` int NOT NULL AUTO_INCREMENT,
  `hs_user_id` int DEFAULT NULL,
  `token` text,
  `iat` datetime DEFAULT NULL,
  `expires` datetime DEFAULT NULL,
  PRIMARY KEY (`hs_token_id`),
  KEY `fk_hs_token_hs_user_idx` (`hs_user_id`),
  CONSTRAINT `fk_hs_token_hs_user` FOREIGN KEY (`hs_user_id`) REFERENCES `hs_user` (`hs_user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=97 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hs_token`
--

LOCK TABLES `hs_token` WRITE;
/*!40000 ALTER TABLE `hs_token` DISABLE KEYS */;
INSERT INTO `hs_token` VALUES (96,1,'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJlbWFpbCI6Im1vaXNlcy1uZ0Bob3RtYWlsLmNvbSIsInBhc3N3b3JkIjoicG1Xa1dTQkNMNTFCZmtobjc5eFB1S0JLSHovL0g2QittWTZHOS9laWV1TT0iLCJoc191c2VyX2lkIjoxLCJpYXQiOjE2MjMwOTg0OTN9.cQZMxNPMddioq9kmX5D6hxk41zbkrjobGZX7euJ2RVU',NULL,NULL);
/*!40000 ALTER TABLE `hs_token` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hs_user`
--

DROP TABLE IF EXISTS `hs_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `hs_user` (
  `hs_user_id` int NOT NULL AUTO_INCREMENT,
  `email` varchar(40) DEFAULT NULL,
  `password` varchar(200) DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`hs_user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hs_user`
--

LOCK TABLES `hs_user` WRITE;
/*!40000 ALTER TABLE `hs_user` DISABLE KEYS */;
INSERT INTO `hs_user` VALUES (1,'moises-ng@hotmail.com','pmWkWSBCL51Bfkhn79xPuKBKHz//H6B+mY6G9/eieuM=','Moises','Quispe');
/*!40000 ALTER TABLE `hs_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_game_settings`
--

DROP TABLE IF EXISTS `user_game_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_game_settings` (
  `user_game_settings_id` int NOT NULL AUTO_INCREMENT,
  `hs_user_id` int DEFAULT NULL,
  `game_difficulty_id` int DEFAULT NULL,
  PRIMARY KEY (`user_game_settings_id`),
  KEY `fk_game_settings_hs_user1_idx` (`hs_user_id`),
  KEY `fk_game_settings_game_difficulty1_idx` (`game_difficulty_id`),
  CONSTRAINT `fk_game_settings_game_difficulty1` FOREIGN KEY (`game_difficulty_id`) REFERENCES `game_difficulty` (`game_difficulty_id`),
  CONSTRAINT `fk_game_settings_hs_user1` FOREIGN KEY (`hs_user_id`) REFERENCES `hs_user` (`hs_user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_game_settings`
--

LOCK TABLES `user_game_settings` WRITE;
/*!40000 ALTER TABLE `user_game_settings` DISABLE KEYS */;
INSERT INTO `user_game_settings` VALUES (1,1,2);
/*!40000 ALTER TABLE `user_game_settings` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-06-07 15:52:11
