const express = require('express')
const router = express.Router()
const gameController = require('../controller/game')
const Authorization = require('../middleware/authorization')

router.post('/', Authorization, gameController.saveGame)

router.get('/difficulty', Authorization, gameController.getDifficulty)

router.get('/settings', Authorization, gameController.getSettings)
router.put('/settings', Authorization, gameController.updateSettings)

router.get('/scores', Authorization, gameController.getScores)


module.exports = router