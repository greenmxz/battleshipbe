const fs = require('fs')
const express = require('express')
const router = express.Router()
const config = require('../config/config')

fs.readdirSync(`${__dirname}/`).forEach( file => {
    let routeName = file.replace('.js','')
    if (routeName !== 'index'){
        router.use(`/${routeName}`,require(`./${routeName}`))
    }
})

router.get('/',
    (req,res) => {
        return res.json(config.app)
    }
)
module.exports = router;