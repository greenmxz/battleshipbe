const express = require('express')
const router = express.Router()
const testController = require('../controller/test')

router.get('/:code', testController.get)
router.get('/', testController.get)

module.exports = router