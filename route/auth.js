const express = require('express')
const router = express.Router()
const authController = require('../controller/auth')
const Authorization = require('../middleware/authorization')


router.post('/login', authController.createToken)

router.get('/token', authController.isLogged)

module.exports = router