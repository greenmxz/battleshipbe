const jwt = require('jsonwebtoken');
const sql = require('../util/db')
const config = require('../config/config')
const util = require('../util/util')

exports.createToken = (email, password, callback) => {
    sql.query('select * from hs_user where email= ? and password= ?', [email, password], (err, res) =>{
        if(err){
            console.log(err)
            callback (err, null);
        }else{
            if (res.length> 0){
                const accessToken = jwt.sign({ email: email,  password: password, hs_user_id: res[0].hs_user_id }, config.token.secretKey);
                sql.query('insert into hs_token (hs_user_id, token) values (?,?)', [res[0].hs_user_id, accessToken], (err, res) =>{
                    if(err){
                        console.log(err)
                        callback (err, null);
                    }else{
                        callback(null, accessToken)
                    }
                    
                });
                
            }else{
                callback (404, 'Invalid credential provided')
            }
            
        }
    })
}

exports.isLogged = (req , callback)=> {
	if (req.header && req.headers.authorization && req.hs_user_id ){
        sql.query(
            "SELECT * FROM hs_token where hs_user_id = ? and token = ? ",
            [req.hs_user_id,req.headers.authorization.split(' ')[1]],
            function (err, result) {
                if (result && result.length>0) {
                    callback(null,'OK')
                } else {
                    callback( 401,  'Not authorized')
                }
            })
	}else{
        callback(401, 'Not authorized')
	}

}





