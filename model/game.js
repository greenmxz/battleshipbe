const sql = require('../util/db')


exports.getDifficulty = (callback) => {
    sql.query('select * from game_difficulty', [], (err, res) =>{
        if(err){
            console.log(err)
            callback (err, null);
        }else{
            callback (null, res)
        }
    })
}

exports.getSettings = (user, callback) => {
    sql.query('select * from user_game_settings inner join game_difficulty on game_difficulty.game_difficulty_id = user_game_settings.game_difficulty_id where hs_user_id=?', [user], (err, res) =>{
        if(err){
            console.log(err)
            callback (err, null);
        }else{
            callback (null, res)
        }
    })
}

exports.updateSettings = (user, body, callback) => {
    sql.query('update user_game_settings set game_difficulty_id = ? where hs_user_id= ?', [body.game_difficulty_id, user], (err, res) =>{
        if(err){
            console.log(err)
            callback (err, null);
        }else{
            callback (null, res)
        }
    })
}

exports.saveGame = (user, body, callback) => {
    sql.query('insert into finished_games (hs_user_id,score,turns) values (?,?,?)', [user, body.score,body.turns], (err, res) =>{
        if(err){
            console.log(err)
            callback (err, null);
        }else{
            callback (null, res)
        }
    })
}

exports.getScores = (user,callback) => {
    sql.query('select * from finished_games  where hs_user_id=? order by turns desc ', [user], (err, res) =>{
        if(err){
            console.log(err)
            callback (err, null);
        }else{
            callback (null, res)
        }
    })
}