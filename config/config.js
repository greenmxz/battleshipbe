const { token } = require('morgan');
const package = require('../package.json')

const environment = process.env.NODE_ENV || 'development';

const config = {}
config.app = {
    name: package.name,
    env: environment
}

require('./'+environment+'_config')(config);

module.exports = config