/*
Configuring the express app
*/
const util = require('../util/util')
const path = require('path')
const express = require('express')
const helmet = require('helmet')
const cors = require('cors')
const bodyParser = require('body-parser')
const expressSanitized = require('express-sanitized')
const sanitizer = require('sanitizer')

const morgan = require('morgan')
const json = require('morgan-json')
var jwt = require('jsonwebtoken');
var Base64 = require('js-base64').Base64;

const loggerFormat = json({
 method: ':method',
 url: ':url',
 status: ':status',
 contentLength: ':res[content-length]',
 responseTime: ':response-time'
})
module.exports = function (argConfig, basePath, controllers){
    const config = require('./config')
    const appBasePath = '/'
    const appRouter = require('../route')

    const app = express()
    //Helmet configuration
    app.use(helmet.frameguard())
    app.use(helmet.noSniff())
    app.use(helmet.hidePoweredBy())
    app.use(helmet.xssFilter())
    app.use(helmet.ieNoOpen())

    //Bodyparser configuration
    app.use(bodyParser.json({
        limit:'2mb'
    }))

    //Sanitize configuration
    app.use(expressSanitized())
    app.use( (req, res, next)=> {
        Object.keys(req.params).map(key => {
            req.paramas[key] = sanitizer.sanitize(req.params[key])
        })
        return next()
    })
    //Cors configuration
    app.use(cors(config.cors))


    //Morgan logger configuration
    app.use(morgan('dev'))
    //SET TOKEN 
    app.use( (req, res, next)=> {
        if (req.originalUrl == '/auth/login'){
            next()
        }else{
            if(req.headers && req.headers.authorization ){
                let parts = req.headers.authorization.split(' ');
                    if (parts.length == 2) { //GET DATA
                        let scheme = parts[0], //bearer
                            credentials = parts[1]; //token
                        let  decoded = jwt.decode(credentials);
                        if (decoded && decoded.hs_user_id){
                            req.hs_user_id = decoded.hs_user_id
                            next()
                        }else{
                            util.sendErrorResponse(res, 400, 'Not authorized','Not authorized')
                        }
                        
                    }else{
                        util.sendErrorResponse(res, 400, 'Not authorized', 'Not authorized')
                    }
            }else{
                util.sendErrorResponse(res, 400, 'Not authorized','Not authorized')
            }
        }
    })
    //Controllers
    app.use(appBasePath,appRouter)



    app.use( (req,res) =>{
        res.status(404)
        return res.json({
            message: 'Resource not found',
            url: sanitizer.sanitize(req.url)
        })
    })
    return app
}
