module.exports = function (config){
    //console.log("Development_config 0:",config)
    config.cors = {
        origin: '*'
    }

    config.db = {
        host: process.env.MYSQL_HOST || '127.0.0.1',
        user: process.env.MYSQL_USER || 'root',
        password: process.env.MYSQL_PASSWORD || 'root',
        database: process.env.MYSQL_DB || 'battleship',
        connectionLimit: 10,
        supportBigNumbers: true,
        killConnection: true,
        timezone:'gmt-5'
    }

    config.https = {
        port: 2302,
    }

    config.token = {
        secretKey: 'my-secret-token'
    }
    //console.log("Development_config:",config)
}


