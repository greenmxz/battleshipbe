const config = require('./config/config')
const http = require('http')

const app = require('./config/app')()

/*
Starting HTTP server!
*/
function main(){
  //For now we will work with http only
  const server = http.createServer(app)
  server.listen(config.https.port, function() {
    console.log('%s listening on %s',config.app.name, config.https.port);
  })
}

if( require.main == module){
  main()
}